extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program("fn foo(a,b,c) {
    let x = a + 1;
    let y = bar(c - b);
    return x * y;
  }
  
  fn bar(a) {
    return a * 3;
  }
  
  fn main() {
    return foo(1,2,3);  
  }");
  println!("{:?}", result);
}
